// ShMem class
// Objective: provide utilities for shared memory in publisher-subscriber model. 
// Principle: High cohesion and low coupling.
//	1.	All modules in Roswell or assigned projects shall use these utilities to publish or subscribe public data.
//	2.	Shared memory here is designed to follow publisher - subscriber model. Any elements in the shared memory can 
//		have only single publisher but multiple subscribers.
//	3.	Publisher can publish its elements at any moments regardless the behaviors of any other publishers or subscribers. 
//		The publisher does not care or know who are the subscribers. Multiple publishers are allowed to publish their own elements in different process/thread simutaneously. 
//	4.	Subscribers can read its subscriptions (the shared elements) at any moments regardless who is writing or reading them. 
//		Multiple subscribers are allowed to read the same elements in different process/thread simutaneously.
//	5.	Every module is allowed to have multiple publishers together with multiple subscribers. 
//	6.	Each shared element always occupies the same location in the shared memory no matter how many times it is declared or loaded in the module.
//	7.	The writing and reading are all operations that are non blocking and multithreaded safe.
//

// MsgQ class
// Objective: provide basic utilities for message queue. 
// Principle: High cohesion and low coupling.
//	1.	All modules in Roswell or assigned projects shall use these utilities to send or receive messages to each other.
//	2.	Message queue defined here follows single reader and multiple writers model. Any message queues can have only single receiver 
//		but multiple senders.
//	3.	Senders can send messages to the destnate receiver at any moments without knowing the state of the receiver and other senders. 
//		Multiple senders are allowed to send their messages to a receiver in different process/thread simutaneously. 
//	4.	Receiver can read its messages at its convenient time. The messages are in FIFO series.
//	5.	Multiple receivers and senders can be defined and worked in single module. 
//	6.	The receiving of a message is a blocking opertion with timeout. It will block until either the message queue has a message 
//		or the timeout expires. The timeout can be specified between 10us to 1s.
//  7. 	The sending of a message is also a blocking operation with timeout. It will block until either the message queue has available 
//		space for the new message or a small 1ms timeout expires.
//	8.	The receiving and the sending of message are all multithreaded safe.
//